
<div class="container-fluid">
   <div class="row">
    <div class="col-lg-3 col-6">
      <!-- small box -->
      <div class="small-box bg-info">
        <div class="inner">
          <h3>{{count($cards)}}</h3>

          <p>Total Number of Submissions</p>
        </div>
        <div class="icon">
          <i class="fas fa-address-card"></i>
        </div>
        <a href="{{route('information.index')}}" class="small-box-footer">View Submissions <i class="fas fa-arrow-circle-right"></i></a>
      </div>
    </div>
    <div class="col-lg-3 col-6">
      <!-- small box -->
      <div class="small-box bg-danger">
        <div class="inner">
          <h3>{{count($BDelivery)}}</h3>

          <p>Total Number of Bank Deliveries</p>
        </div>
        <div class="icon">
          <i class="fa fa-users"></i>
        </div>
        <a href="{{route('information.index')}}" class="small-box-footer">View Submissions <i class="fas fa-arrow-circle-right"></i></a>
      </div>
    </div>

    <div class="col-lg-3 col-6">
      <!-- small box -->
      <div class="small-box bg-success">
        <div class="inner">
          <h3>{{count($HDelivery)}}</h3>

          <p>Total Number of Home Deliveries</p>
        </div>
        <div class="icon">
          <i class="ion ion-person-add"></i>
        </div>
        <a href="{{route('information.index')}}" class="small-box-footer">View Submissions <i class="fas fa-arrow-circle-right"></i></a>
      </div>
    </div>
     <!-- /.col-md-6 -->
      <div class="col-lg-3 col-6">
       <!-- small box -->
       <div class="small-box bg-warning">
         <div class="inner">
           <h3>{{count($users)}}</h3>

           <p>Total Number of USers</p>
         </div>
         <div class="icon">
           <i class="fa fa-users"></i>
         </div>
         <a href="{{route('users.index')}}" class="small-box-footer">View Users <i class="fas fa-arrow-circle-right"></i></a>
       </div>
     </div>
   </div>
   <!-- /.row -->
 </div><!-- /.container-fluid -->
