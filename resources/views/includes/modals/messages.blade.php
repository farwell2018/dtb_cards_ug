@php
    $flashMessage = session()->get('submission_success');
@endphp

<div class="modal fade bd-example-modal-lg " id="ModalMessage" tabindex="-1" role="dialog" aria-labelledby="ModalMessage" aria-hidden="true">

  <div class="modal-dialog notification-dialog  modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-body">

          <div class="row">
            <div class="col-12">
              <div class="noti-header"><h4 class="text-center  noti-success-header" > </h4></div>
              <div class="col light">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 noti-text noti-success" >
                  <h4 class="text-center  noti-success-header" > {!! isset($flashMessage) ? $flashMessage['content'] : "" !!}</h4>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 noti-sbutt" >

                  <button type="button" class="btn dismiss-modal modal-accept noti-sbutton submission-overall" style="color:#fff" data-dismiss="modal"  onclick="closeModal()" >Dismiss</button>

                </div>
              </div>
            </div>
        </div>
      </div>
    </div>
  </div>
</div>
