  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="{{route('home')}}" class="brand-link">
      <img src="{{asset('images/DTB Logo-01_9.png')}}" alt="TCC" class="brand-image "
           style="opacity: .8">
      <span class="brand-text font-weight-light"><br></span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <!-- <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="dist/img/user2-160x160.jpg" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          <a href="#" class="d-block">Alexander Pierce</a>
        </div>
      </div> -->

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          <?php
                if(Auth::user()->role_id === 1){

                   $navs = config('admin-constants.admin_nav');
                }elseif(Auth::user()->role_id === 2){
                     $navs = config('admin-constants.BankAssociate_nav');
                }
                


            foreach ($navs as $label => $nav) :
              ?>

            @foreach($nav as $el)
       <?php
    // Program to display current page URL.

        $link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ?
                    "https" : "http") . "://" . $_SERVER['HTTP_HOST'] .
                    $_SERVER['REQUEST_URI'];
          ?>

          @if (strcmp($link, route($el['route'])))
          <li class="nav-item">
            <a href="{{route($el['route'])}}" class="nav-link">
              {!! $el['icon'] !!}
              <p>
               {{ $el['title'] }}
              </p>
            </a>
          </li>
          @else
          <li class="nav-item">
            <a href="{{route($el['route'])}}" class="nav-link">
              {!! $el['icon'] !!}
              <p>
               {{ $el['title'] }}
              </p>
            </a>
          </li>
         @endif
         @endforeach
        <?php endforeach;?>
        
     
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>
