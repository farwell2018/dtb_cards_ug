<!-- Background Image Parallax -->
<div class="jarallax">
    <img class="jarallax-img" src="{{$image}}" alt="">
    <div class="content">
      <div class="wrap">
        <h1>{!! $text !!}</h1>
      </div>
      
    </div>
    <a href="#next-section" title="" class="scroll-down-link " aria-hidden="true" >
      <i class="fas fa-chevron-down"></i>
    </a>
</div>
<div id="next-section"></div>
