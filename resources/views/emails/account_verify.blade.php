@extends('layouts.email')

@section('content')


<p style="font-family:poppins, sans-serif;font-size: 28px;line-height:1.6;font-weight:normal;margin:0 0 30px;padding:0;color:#e71a3d;text-align:center;">Login Verification</p>
<hr class="line-footer">
<p class="bigger-bold" style="font-family: poppins, sans-serif;font-size: 18px;line-height: 1.6;font-weight: normal;margin: 30px 0 30px;padding: 0;color:#7C7C7C;text-align:center;">Hello {{ ucwords(strtolower($name)) ?: '' }},</p>

<p style="font-family: poppins, sans-serif;font-size: 18px;line-height: 1.6;font-weight: normal;margin: 30px 0 30px;padding: 0;color:#7C7C7C;text-align:center;">
    Your are attempting to login to  {{ config('app.name', 'The Portal') }} </p>

  <p style="font-family: poppins, sans-serif;font-size: 18px;line-height: 1.6;font-weight: normal;margin: 30px 0 30px;padding: 0;color:#7C7C7C;text-align:center;">

      In order to complete your login enter the verification code below <br>

      <span style="color:#23245F;font-weight: bold;">{{$code}}</span> </p>
  
<p style="font-family: poppins, sans-serif;font-size: 18px;line-height: 1.6;font-weight: normal;margin: 30px 0 30px;padding: 0;color:#7C7C7C;text-align:center;">
 Kindly ignore this email incase you did not make this request.
</p>
<p style="font-family: poppins, sans-serif;font-size: 18px;line-height: 1.6;font-weight: bold;margin: 20px 0 0;padding: 0;color:#7C7C7C;text-align:center;">
Best Regards,
</p>
<p style="font-family:poppins, sans-serif;font-size: 18px;line-height: 1.6;font-weight: bold;margin: 0 0 10px;padding: 0;color:#7C7C7C;text-align:center;">
  DTB Team
</p>
{{-- 
<p style="font-family:poppins, sans-serif;font-size: 18px;line-height: 1.6;font-weight: bold;margin: 0 0 10px;padding: 0;color:#662F8E;text-align:center;">
 support@farwell-consultants.com
</p> --}}


@endsection
