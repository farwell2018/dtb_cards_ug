@extends('layouts.app')

@section('title','Card Delivery')

@section('content')
@if(Session::has('submission_success'))
<script>
jQuery(document).ready(function($){
    console.log('it has');
 $("#ModalMessage").addClass('show');
});
</script>
@endif
<div class="content-page">
    @include('includes.components.parallax',[
        'image'=>asset("images/New_card_banner.jpg"),
        'text'=>'Enter your card delivery information below',
        'page'=>'Courses'
        ])

<div class="container justify-middle">
    <div class="clearfix"><br><br></div>
    <div class="row">
    @if (session('success'))
    <div class="col-md-12">
        <div class="row">
    <div class="alert alert-success " role="alert" style="margin-top:1%;">
        {{ session('success') }}
    </div>
</div>
</div>
 @endif
 <div class="col-md-12">
    <div class="row mobile_row">
    <form class="form-horizontal" method="POST" action="{{ route('card.information') }}">
        {{ csrf_field() }}

        <fieldset class="scheduler-border">
            <legend class="scheduler-border">Personal Information</legend>
            <p>Fill in your personal information below</p>

            <div class="col-md-12">
                <div class="row">
                   <div class="col-md-6 block-spacing">
                     <input id="name" type="text" class="form-control" name="first_name" value="{{ old('first_name') }}" required autofocus placeholder="First Name">

                     @if ($errors->has('first_name'))
                         <span class="help-block">
                             <strong>{{ $errors->first('first_name') }}</strong>
                         </span>
                     @endif
                   </div>

                   <div class="col-md-6 mobile-spacing">
                     <input id="name" type="text" class="form-control" name="last_name" value="{{ old('last_name') }}" required autofocus placeholder="Last Name">

                     @if ($errors->has('last_name'))
                         <span class="help-block">
                             <strong>{{ $errors->first('last_name') }}</strong>
                         </span>
                     @endif

                   </div>


                </div>

             </div>
             <div class="col-md-12">
                <div class="row">
                   <div class="col-md-6 block-spacing">
                     <input id="name" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus placeholder="Email Adress">

                     @if ($errors->has('email'))
                         <span class="help-block">
                             <strong>{{ $errors->first('email') }}</strong>
                         </span>
                     @endif
                   </div>

                   <div class="col-md-6 mobile-spacing">
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <div class="choices" data-type="select-one" tabindex="0" role="combobox" aria-autocomplete="list" aria-haspopup="true" aria-expanded="false">
                                <div class="choices__inner">
                        <select class="form-select downArrow choices__input" aria-label="Country Code" name="mobile_number_country" id="mobile_number_country"  disabled>
                            <option value="+256">Ug (+256)</option>
                        </select>
                                </div>
                            </div>
                        </div>
                        <input id="mobile_number" type="number" class="form-control mobile-field" name="mobile_number" value="{{ old('mobile_number') }}" placeholder="700000000" required="" autocomplete="mobile_number">
                    </div>

                    @if ($errors->has('phone_number'))
                    <span class="help-block">
                        <strong>{{ $errors->first('phone_number') }}</strong>
                    </span>
                @endif

                   </div>


                </div>

             </div>
        </fieldset>

        <fieldset class="scheduler-border">
            <legend class="scheduler-border">Account Details</legend>
            <p>Fill in your account information below</p>
            <div class="col-md-12 ">
                <div class="row">
                    <div id="requirements">
                        <div class="col-md-12 ">
                        <div class="row">
                            <div class="col-md-6 block-spacing">

                            <select name="Question[1][Question]" id="Branch_Question_1" class="form-control method_select" required>
                                <option value="">Select branch where you opened your account</option>
                                @foreach($branches as $branch)
                                 <option value="{{$branch->id}}"> {{$branch->name}}</option>

                                @endforeach

                             </select>

            </div>
            <div class="col-md-6 mobile-spacing">

                  <input   name="Question[1][Answers][1]" id="PreQualAnswers_Answers_1" type="text" value="" class="form-control" placeholder="Account Number" required>

            </div>
            </div>
                        </div>

              </div><!--requirements-->

                        <div class=" form-group">

                            <input type="button" value="+Add account in different branch" class="btn btn-primary btn-over pull-right add_button mobile_margin" id="addquestion" style="margin-top:0px"/>
                          <input type="button" value="+Add account within same branch" class="btn btn-primary  btn-over pull-right add_button" id="addanswer" style="margin-top:0px;"/>
                        </div>

                </div>

            </div>
        </fieldset>

        <fieldset class="scheduler-border">
            <legend class="scheduler-border">Delivery Details</legend>
            <p>How would you like to get your DTB card(s)?</p>
            <div class="col-md-12 ">
                <div class="row">
                    <div class="col-md-6 block-spacing">
                        <select name="delivery_method" id="delivery_method" class="form-control method_select" required>
                            <option value=" ">Select Delivery Method</option>
                            <option value="1">Happy to collect it from my nearest Branch</option>
                            <option value="2">Deliver it to my home/office</option>

                         </select>
                    </div>
                        <div id="branch_text" style="display:none">
                            <div class="col-md-6 mobile-spacing">
                                <select name="branch" id="branch" class="form-control method_select">
                                   <option value="">Select Branch</option>
                                   @foreach($branches as $branch)
                                    <option value="{{$branch->id}}"> {{$branch->name}}</option>
                                   @endforeach
                                </select>
                        </div>
                    </div>

                        <div id="home_text" style="display:none">
                            <div class="col-md-6 mobile-spacing">
                                <input id="name" type="text" class="form-control" name="home" value="{{ old('home') }}"  placeholder="Please enter your home/office address e.g Estate Name, Building Name, House Number">
                            </div>
                        <div class="col-md-12 ">
                            <div class="row">
                             <!-- ---------Select City or Town to Deliver to  -->
                            <div id="regions_list" class="col-md-6 block-spacing">
                                <select name="region" id="region" class="form-control method_select" >
                                        <option value="">Select Region</option>
                                    @foreach($regions as $region)
                                        <option value="{{ $region->id }}">{{ $region->region }}</option>
                                    @endforeach

                                </select>
                            </div>
                            <!-----------End of City/Town Selection----------------- -->

                            @unless(empty($regions))
                                <div id="cities_list" class="col-md-6 block-spacing" style="display:none">
                                    <select name="city" id="city_name" class="form-control method_select" >

                                    </select>
                                </div>
                            @endunless
                            </div>
                        </div>

                        <div class="col-md-12 ">
                            <div class="row">
                                <div class="col-md-6 block-spacing">
                                    <input id="name" type="text" class="form-control" name="road_near" value="{{ old('road_near') }}"  placeholder="Road near prefered delivery location">
                            </div>
                                <div id="home_text" class="col-md-6 block-spacing">
                                    <input id="name" type="text" class="form-control" name="landmark_near" value="{{ old('landmark_near') }}"  placeholder="Landmark near prefered delivery location">
                            </div>
                            </div>
                        </div>
                        </div>
                </div>

            </div>
        </fieldset>
        <div class="form-group">
            <div class="col-md-12 ">
                 <div class="row mobile_row">
                    <div class="col-md-6 block-spacing">
                            <span class="captcha-image" style="margin-left:10px">{!! Captcha::img() !!}</span> &nbsp;&nbsp;
                            <button type="button" class="btn btn-success refresh-button">Refresh</button>
                            <input id="captcha" type="text" class="form-control " name="captcha" style="margin-top:8px" required placeholder="Enter the characters in the box above">

                            @if ($errors->has('captcha'))
                            <span class="help-block">
                                <strong>{{ $errors->first('captcha') }}</strong>
                            </span>
                        @endif

                    </div>

                    <div class="col-md-6">


                <button type="submit" class="btn btn-primary btn-overall pull-right">
                    Submit
                 </button>
                    </div>

                 </div>

            </div>
        </div>
    </form>
    </div>
</div>
</div>
</div>
</div>
@endsection

@section('js')
<script>

$(document).ready(function(){

    $('#delivery_method').on('change', function() {
  var type = $(this).val();
  if (type == 1) {
    document.getElementById("branch_text").style.display = "block";
    document.getElementById("home_text").style.display = "none";
  } else if(type == 2) {
    document.getElementById("branch_text").style.display = "none";
    document.getElementById("home_text").style.display = "block";
  }

});

$('#region').on('change', function(){
    var id = $(this).val();
    if(id){
        $.ajax({
        url: '/cities/' + id,
        type: "Get",
        dataType: 'json',
        data:{id:$(id).val()},
            success: function(response){
                if(response != null){
                    $('#cities_list').show();

                    const targetNode = document.getElementById('city_name');

                    targetNode.innerHTML = response.reduce((options, {id, city_name}) =>
                        options+=`<option value="${id}">${city_name}</option>`,
                        '<option value="0" selected>Select City</option>');
                }else {
                    $('#cities_list').hide();
                }
            }
        });
    }
});

});
</script>

<script type="text/javascript">
    $(document).ready(function() {
        $('.refresh-button').click(function() {
            $.ajax({
                type: 'get',
                url: '{{ route('refreshCaptcha') }}',
                success:function(data) {

                    console.log(data);
                    $('.captcha-image').html(data);
                }
            });
        });
    });
</script>

<script>
    $(document).ready(function(){

        var myJSON = {!!$branches!!};

        if( typeof phpAnsId !== 'undefined' ) {
      var ansId =phpAnsId;
      var QId = phpQId;

  }
  else{
  var ansId =2;
  }
        if( typeof phpQuestId !== 'undefined' ) {
      var questId =phpQuestId;
  }
  else{
  var questId =1;
  }
  if( typeof phpAnssId !== 'undefined' ) {
      var anssId =phpAnssId;
  }
  else{
  var anssId =2;
  }
   $('#addanswer').click( function(j) {


       $("#requirements").append(

                       '  <div class="col-md-12 "><div class="row"> <div class="col-md-6 block-spacing"></div>'+
                       ' <div class="col-md-6 mobile-spacing">'+
                       '<input  name="Question['+questId+'][Answers]['+ansId+']" id="PreQualAnswers_Answers_'+ansId+'" type="text" value="" class="form-control" placeholder="Account Number">'+
                       '</div></div></div>'

       );
       ansId++;

       });


   $('#addquestion').click( function(j) {
       questId++;

       var data = '</br>'+
                '  <div class="col-md-12 "><div class="row"> <div class="col-md-6 block-spacing">'+
                    '<select name="Question['+questId+'][Question]" id="Branch_Question_'+questId+'" class="form-control method_select" >'+

                '<option value="">Select Branch</option>';

        $.each(myJSON, function(key, value) {
        data+= '<option value="'+ value.id + '">' + value.name + '</option>';

        });

      data+=  '</select>'+
                '</div>'+
                ' <div class="col-md-6 mobile-spacing">'+
                '<input  name="Question['+questId+'][Answers]['+ansId+']" id="PreQualAnswers_Answers_'+ansId+'" type="text" value="" class="form-control" placeholder="Account Number">'+
                '</div></div></div>'


      $("#requirements").append(data);
      ansId++;

      });
    });
</script>
@endsection