<?php
/**
* Created by PhpStorm.
* User: josin
* Date: 20/01/2018
* Time: 11:08
*/
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
  

</head>
<body paddingwidth="0" paddingheight="0" bgcolor="#d1d3d4"
style="padding-top: 0; padding-bottom: 0; padding-top: 0; padding-bottom: 0; background-repeat: repeat; width: 100% !important; -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; -webkit-font-smoothing: antialiased;"
offset="0" toppadding="0" leftpadding="0">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tbody>
    <tr>
      <td>
        <table width="800" border="0" cellspacing="0" cellpadding="0" align="center" bgcolor="#ffffff" class="MainContainer">
          <!-- =============== START HEADER =============== -->
          <tbody>
            <tr>
              <td>
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                  <tbody>
                    <tr>
                      <td>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                          <tbody>
                            <tr>
                              <td class="movableContentContainer">
                                <!-- =============== START BODY =============== -->

                                <div class="movableContent"
                                style="border: 0px; padding-top: 0px; position: relative;">


                              </div>
                              <div class="movableContent" style="border: 0px; padding-top: 0px; position: relative;color:#fff; width:700px;">
                              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tbody>
                                  <tr>
                                    <td>
                                      <table width="100%" border="0" cellspacing="0" cellpadding="0" style="width:660px; margin-left:20px; background:#ffffff; color:#000;margin-bottom: 40px;">
                                      <tbody>
                                        <tr>
                                          <td valign="top" width="40">&nbsp;</td>
                                          <td>
                                            <table width="100%" border="0"
                                            cellspacing="0" cellpadding="0"
                                            align="center">
                                            <tr>
                                              <td height='25'></td>
                                            </tr>
                                            <tr>
                                              <td>
                                                <div class='contentEditableContainer contentTextEditable'>
                                                  <div class='contentEditable'
                                                  style='text-align: left;'>

                                                  {{--content--}}

                                                  @yield('content')

                                                </div>
                                              </div>
                                            </td>
                                          </tr>

                                      </table>
                                    </td>
                                  </tr>
                                </tbody>
                              </table>
                            </td>
                          </tr>
                        </tbody>
                      </table>


                      <!-- =============== END BODY =============== --
                      <!-- =============== START FOOTER =============== -->

                      <div class="movableContent"
                      style="border: 0px; background-color: #fff; padding-top: 0px; position: relative;">
                      <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tbody>
                          <tr>
                            <td height="20"></td>
                          </tr>
                         
                          <tr>
                            <td height="20"></td>
                          </tr>
                        </tbody>
                      </table>


                    </div>

                    <!-- =============== END FOOTER =============== -->
                  </td>
                </tr>
              </tbody>
            </table>
          </td>
        </tr>
      </tbody>
    </table>
  </td>
</tr>
</tbody>
</table>
</td>
</tr>
</tbody>
</table>



</body>
</html>
