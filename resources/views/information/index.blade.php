@extends('layouts.admin')

@section('header')
<div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h4 class="m-0 text-dark"> Customers' Card Delivery Details</h4>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{route('home')}}">Home</a></li>
              <li class="breadcrumb-item active">Submissions </li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
        <div class="row mb-2">
          <div class="col-sm-11">
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->

@endsection

@section('content')
     <div class="container-fluid">
         <div class="row">
            <div class="col-md-12">
            <div class="card">
                <!-- /.card-header -->
                <div class="kt-form" id="filterForm"	>
                <div class="card-body" style="padding: 1rem;">
                    <div class="kt-portlet__head">
                        <div class="kt-portlet__head-label">
                            <h5 class="kt-portlet__head-title">
                                Filters
                            </h5>
                        </div>
                    </div>

                    <div class="row">
                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                      <div class="form-group">
                        <label for="course">Delivery Method</label>
                        <select class="form-control" name="delivery">
                          <option value="">Select Delivery Method</option>
                          <option value="1">Collect from branch</option>
                          <option value="2">Door Deliver</option>
                        </select>
                      </div>
                    </div>

                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                      <div class="form-group">
                        <label for="course">Branch Delivery</label>
                        <select class="form-control" name="branch">
                          <option value="">Select Branch Delivery</option>
                          @foreach($branches as $branch)
                          <option value="{{$branch->id}}">{{$branch->name}}</option>
                          @endforeach
                        </select>
                      </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                      <div class="form-group">
                        <label for="daterange">Date Range</label>
                        <input type="text" name="daterange" id="daterange" class="form-control" value="" placeholder="Click to select Date range"/>
                      </div>
                    </div>

                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 pull-right">
                        <button class="btn btn-primary pull-right" onclick="filterReports()" style="margin-top: 8%;">Submit</button>
                    </div>
                  </div>


                    </div>
            </div>
                <!--end::Form-->
            </div>

        </div>

         </div>
        <div class="row">
         <div class="col-12">
            <div class="card">
              <!-- /.card-header -->
              <div class="card-body">
               <table class="table table-bordered table-striped table-hover" id="information">
                <thead>
                  <tr>
                    <th>#</th>
                    <th>Delivery Service</th>
                    <th>First Name</th>
                    <th>Last Name</th>
                    <th>Street</th>
                    <th>City/Town</th>
                    <th>Region</th>
                    <th>Phone Number</th>
                    <th>Email</th>
                    <th>Bank Branch</th>
                    <th>Account Number</th>
                    <th>Location</th>
                    <th>Landmark Near</th>
                    <th>Created At</th>

                  </tr>
                </thead>
              </table>
              </div>
            </div>
          </div>

        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
@endsection

@section('footer-js')
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />


<script>

var dataTable;


//Set ajax baseURl
let baseUrl = "{!! route('information.json') !!}?";

//Query data
function encodeQueryData(data) {
 const ret = [];
 for (let d in data)
   ret.push(encodeURIComponent(d) + '=' + encodeURIComponent(data[d]));
 return ret.join('&');
}

//Filter method
function filterReports(){
  //Get Status value
  const delivery = document.querySelector('#filterForm select[name=delivery]').value

  const branch = document.querySelector('#filterForm select[name=branch]').value
  //Get date range
  const range = document.querySelector('#filterForm input[name=daterange]').value
  //Add to URL
  const url = baseUrl + "&" + encodeQueryData({delivery,range,branch})

  dataTable.ajax.url( url ).load();
}

$(document).ready(function(){

//Start daterange plugin
$('input#daterange').daterangepicker({autoUpdateInput: false});

$('input#daterange').on('apply.daterangepicker', function(ev, picker) {
    $(this).val(picker.startDate.format('MM/DD/YYYY') + ' - ' + picker.endDate.format('MM/DD/YYYY'));
});

$('input#daterange').on('cancel.daterangepicker', function(ev, picker) {
    $(this).val('');
});

dataTable = $('#information').DataTable({
    responsive: false,
    "deferRender": true,
    "processing": true,
    "serverSide": true,
    "ordering": true, //disable column ordering
    "lengthMenu": [
      [5, 10, 15, 20, 25, -1],
      [5, 10, 15, 20, 25, "All"] // change per page values here
    ],
    "pageLength": 100,
    "scrollX": true,
    "autoWidth": false,
    "fixedHeader": {
        "header": false,
        "footer": false
    },
    "columnDefs": [
      { "width": "70px", "targets": 0 },
      { "width": "70px", "targets": 1 },
      { "width": "70px", "targets": 2 },
      { "width": "70px", "targets": 3 },
      { "width": "70px", "targets": 4 },
      { "width": "70px", "targets": 5 },
      { "width": "70px", "targets": 6 },
      { "width": "70px", "targets": 7 },
      { "width": "70px", "targets": 8 },
      { "width": "70px", "targets": 9 },
      { "width": "70px", "targets": 10 },
      { "width": "70px", "targets": 11 },
      { "width": "70px", "targets": 12 },
      { "width": "70px", "targets": 13 },
],
fixedColumns: true,
    "ajax": {
      url: baseUrl,
      method: 'GET'
    },
    // dom: '<"html5buttons"B>lTfgitp',
    "dom": "<'row' <'col-md-12'>><'row'<'col-md-8 col-sm-12'lB><'col-md-4 col-sm-12'f>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>", // horizobtal scrollable datatable
    buttons: [

      {extend: 'excel', title: '{{ config('app.name', 'DTB') }} - List of Submissions',exportOptions: {columns: [0, 1, 2,3,4,5,6,7,8,9,10,11, 12, 13]}},

  ],
  columns: [
    {data: 'DT_RowIndex', name: 'DT_RowIndex', orderable: true, searchable: true},
    {data: 'delivery_method', name: 'delivery_method', orderable: false, searchable: true},
    {data: 'first_name', name: 'first_name', orderable: false, searchable: true},
    {data: 'last_name', name: 'last_name', orderable: false, searchable: true},
    {data: 'road_near', name: 'road_near', orderable: false, searchable: true},
    {data: 'city', name: 'city', orderable: false, searchable: true},
    {data: 'region', name: 'region', orderable: false, searchable: true},
    {data: 'phone_number', name: 'phone_number', orderable: false, searchable: true},
    {data: 'email', name: 'email', orderable: false, searchable: true},
    {data: 'branch', name: 'branch', orderable: false, searchable: true},
    {data: 'account_number', name: 'account_number', orderable: false, searchable: true},
    {data: 'location', name: 'location', orderable: false, searchable: true},
    {data: 'landmark_near', name: 'landmark_near', orderable: false, searchable: true},
    {data: 'created_at', name: 'created_at', orderable: false, searchable: true},

  ],
});
});

</script>


<script>
$(document).ready(function() {
    $(".buttons-excel").click(function(){

      var user = {{Auth::user()->id}};
      var url = "{{ route('logs.store') }}";
      var token = "{{ csrf_token() }}";

      var formData = {
        'download_by': user,
        '_token': token,
    };


    $.ajax({
        url: url,
        type: 'POST',
        dataType: 'JSON',
        data: formData,
        success: function(res) {

        },

    });
    });
});

</script>

@endsection