@extends('layouts.auth')

@section('title','Login')


@section('content')

<div class="login-page">
<div class="login-box">
 
  <!-- /.login-logo -->
  <div class="card">
    <div class="card-body login-card-body">
     
      <h2>You don't have permissions to access this page</h2>
      <!-- /.social-auth-links -->
    </div>
    <!-- /.login-card-body -->
  </div>
</div>
</div>
@endsection
