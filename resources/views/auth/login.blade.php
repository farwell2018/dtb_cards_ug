@extends('layouts.auth')

@section('title','Login')


@section('content')

<div class="login-page">
<div class="login-box">
  <div class="login-logo">
    <a href="#">{{ __('Login') }}</a>
  </div>
  <!-- /.login-logo -->
  <div class="card">
    <div class="card-body login-card-body">
      <p class="login-box-msg">Sign in to start your session</p>

      <form method="POST" action="{{ route('login') }}">
        {{ csrf_field() }}
        <div class="input-group mb-3">
          <input type="email" class="form-control " placeholder="Email" value="{{ old('email') }}" required autocomplete="email" autofocus name="email">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-envelope"></span>
            </div>
          </div>
          @if ($errors->has('email'))
          <span class="help-block">
              <strong>{{ $errors->first('email') }}</strong>
          </span>
      @endif
        </div>
        <div class="input-group mb-3">
          <input id="password" type="password" class="form-control " name="password" required autocomplete="current-password">

          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-lock"></span>
            </div>
          </div>

          @if ($errors->has('password'))
          <span class="help-block">
              <strong>{{ $errors->first('password') }}</strong>
          </span>
      @endif
        </div>
        <div class="row">
        
          </div>
          <!-- /.col -->
          <div class="col-12">
            <button type="submit" class="btn btn-primary pull-right">
                {{ __('Login') }}
            </button>
          </div>
          <!-- /.col -->
        </div>

    @if (session('error'))
        <div class="alert alert-danger " role="alert" style="margin-top:1%;margin-left: 5%;margin-right: 5%;">
            {{ session('error') }}
        </div>
    @endif
      </form>
      <!-- /.social-auth-links -->
    </div>
    <!-- /.login-card-body -->
  </div>
</div>
</div>
@endsection
