@extends('layouts.auth')

@section('title','Verification')


@section('content')

<div class="login-page">
<div class="login-box">
  <div class="login-logo">
    <a href="#">{{ __('Account Verification') }}</a>
  </div>
  <!-- /.login-logo -->
  <div class="card">
    <div class="card-body login-card-body">
      <p class="form-head"> Please enter the verification code sent to your email address: </p>

    <form method="POST" action="{{ route('site-account_verification') }}" id="registration-form"
        class="form-horizontal">
        {{ csrf_field() }}
        <input type="hidden" name="user_id" value="{{ $id }}" />

          <div class="input-group mb-3">
            <input type="text" class="form-control " placeholder="Verification Code" value="{{ old('code') }}" required  autofocus name="code" id="register-form-code">
            <div class="input-group-append">
              <div class="input-group-text">
                <span class="fas  fa-lock"></span>
              </div>
            </div>
            @if ($errors->has('code'))
            <span class="help-block">
                <strong>{{ $errors->first('code') }}</strong>
            </span>
        @endif
          </div>
       


        <button type="submit" class="btn btn-primary pull-right">Verify</button>
    </form>
      <!-- /.social-auth-links -->
    </div>
    <!-- /.login-card-body -->
  </div>
</div>
</div>
@endsection
