@extends('layouts.admin')

@section('header')
<div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Edit: {{ $user->first_name  }}</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{route('home')}}">Home</a></li>
              <li class="breadcrumb-item active"><a href="{{route('users.index')}}">Users </a></li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->

@endsection

@section('content')
     <div class="container-fluid">
        <div class="row">
         <div class="col-12">
            <div class="card">
              <!-- /.card-header -->
              <div class="card-body">

               <form role="form" method="post" action="{{ route('users.update',$user->id) }}">
                 @method('PUT')
                 @csrf
                <div class="card-body">
                  <div class="form-group">
                    <label for="exampleInputEmail1">First Name</label>
                    <input type="text"  class="form-control{{ $errors->has('first_name') ? ' is-invalid' : '' }}" name="first_name" value="{{ $user->first_name  }}" required>

                    @if ($errors->has('first_name'))
                    <span class="invalid-feedback" role="alert">
                      <strong>{{ $errors->first('first_name') }}</strong>
                    </span>
                    @endif
                  </div>
                  <div class="form-group">
                    <label for="exampleInputEmail1">Last Name</label>
                    <input type="text"  class="form-control{{ $errors->has('last_name') ? ' is-invalid' : '' }}" name="last_name" value="{{ $user->last_name  }}" required>

                    @if ($errors->has('last_name'))
                    <span class="invalid-feedback" role="alert">
                      <strong>{{ $errors->first('last_name') }}</strong>
                    </span>
                    @endif
                  </div>

                  <div class="form-group">
                    <label for="exampleInputEmail1">Email</label>
                    <input type="text"  class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ $user->email  }}" required>

                    @if ($errors->has('email'))
                    <span class="invalid-feedback" role="alert">
                      <strong>{{ $errors->first('email') }}</strong>
                    </span>
                    @endif
                  </div>
                  <div class="form-group">
                    <label for="exampleInputEmail1">Password</label>
                    <input type="password"  class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" value="" >
                    @if ($errors->has('password'))
                    <span class="invalid-feedback" role="alert">
                      <strong>{{ $errors->first('password') }}</strong>
                    </span>
                    @endif
                  </div>
              
                      <div class="form-group ">
                        <label for="exampleInputEmail1">Role</label>
                            <select id="role" name="role" class="form-control{{ $errors->has('role') ? ' is-invalid' : '' }}" required>
                            <option disabled selected>Select role</option>
                            @foreach($roles as $role)
                            @if($role['id'] === $user->role_id)
                              <option value="{{$role->id}}" selected="selected">{{$role->name}}</option>
                              @else
                             <option value="{{$role->id}}">{{$role->name}}</option>
                              @endif
                            @endforeach
                            </select>

                                @if ($errors->has('role'))
                                <span class="invalid-feedback" role="alert">
                                  <strong>{{ $errors->first('role') }}</strong>
                                </span>
                                @endif

                          </div>


                          <div class="form-group ">
                            <label for="exampleInputEmail1">Status</label>
                                <select id="status" name="status" class="form-control{{ $errors->has('status') ? ' is-invalid' : '' }}" required>
                                <option disabled selected>Select Status</option>
                                @foreach($statuses as $status)
                                @if($status['id'] === $user->status)
                                  <option value="{{$status['id']}}" selected="selected">{{$status['name']}}</option>
                                  @else
                                 <option value="{{$status['id']}}">{{$status['name']}}</option>
                                  @endif
                                @endforeach
                                </select>
    
                                    @if ($errors->has('status'))
                                    <span class="invalid-feedback" role="alert">
                                      <strong>{{ $errors->first('status') }}</strong>
                                    </span>
                                    @endif
    
                              </div>
                          
                </div>
                <!-- /.card-body -->
                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Submit</button>
                </div>
              </form>
              </div>
            </div>
          </div>

        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
@endsection
