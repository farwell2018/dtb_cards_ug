@extends('layouts.admin')

@section('header')
<div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Create User</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{route('home')}}">Home</a></li>
              <li class="breadcrumb-item active"><a href="{{route('users.index')}}">Users </a></li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->

@endsection

@section('content')
     <div class="container-fluid">
        <div class="row">
         <div class="col-12">
            <div class="card">
              <!-- /.card-header -->
              <div class="card-body">
               
               <form role="form" method="post" action="{{route('users.store')}}">
                {{ csrf_field() }}
                <div class="card-body">
                  <div class="form-group">
                    <label for="exampleInputEmail1">First Name</label>
                    <input type="text"  class="form-control{{ $errors->has('first_name') ? ' is-invalid' : '' }}" name="first_name" value="{{ old('first_name')  }}" required>

                    @if ($errors->has('first_name'))
                    <span class="invalid-feedback" role="alert">
                      <strong>{{ $errors->first('first_name') }}</strong>
                    </span>
                    @endif
                  </div>
                  <div class="form-group">
                    <label for="exampleInputEmail1">Last Name</label>
                    <input type="text"  class="form-control{{ $errors->has('last_name') ? ' is-invalid' : '' }}" name="last_name" value="{{ old('last_name')  }}" required>

                    @if ($errors->has('last_name'))
                    <span class="invalid-feedback" role="alert">
                      <strong>{{ $errors->first('last_name') }}</strong>
                    </span>
                    @endif
                  </div>

                  <div class="form-group">
                    <label for="exampleInputEmail1">Email Address</label>
                    <input type="email"  class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email')  }}" required>

                    @if ($errors->has('email'))
                    <span class="invalid-feedback" role="alert">
                      <strong>{{ $errors->first('email') }}</strong>
                    </span>
                    @endif
                  </div>

                  <div class="form-group">
                    <label for="exampleInputEmail1">Password</label>
                    <input type="password"  class="form-control" name="password" value="{{ old('password')  }}" required>

                    @if ($errors->has('password'))
                    <span class="invalid-feedback" role="alert">
                      <strong>{{ $errors->first('password') }}</strong>
                    </span>
                    @endif
                  </div>

                  <div class="form-group ">
                    <label for="exampleInputEmail1">Role</label>
                        <select id="role" name="role" class="form-control{{ $errors->has('role') ? ' is-invalid' : '' }}" required>
                        <option disabled selected>Select role</option>
                        @foreach($roles as $role)
                       
                         <option value="{{$role->id}}">{{$role->name}}</option>
                          
                        @endforeach
                        </select>
  
                            @if ($errors->has('role'))
                            <span class="invalid-feedback" role="alert">
                              <strong>{{ $errors->first('role') }}</strong>
                            </span>
                            @endif
  
                      </div>

                      <div class="form-group ">
                        <label for="exampleInputEmail1">Status</label>
                            <select id="role" name="status" class="form-control{{ $errors->has('status') ? ' is-invalid' : '' }}" required>
                            <option disabled selected>Select status</option>
                            @foreach($statuses as $status)
                           
                             <option value="{{$status['id']}}">{{$status['name']}}</option>
                              
                            @endforeach
                            </select>
      
                                @if ($errors->has('status'))
                                <span class="invalid-feedback" role="alert">
                                  <strong>{{ $errors->first('status') }}</strong>
                                </span>
                                @endif
      
                          </div>
                </div>

               
                <!-- /.card-body -->
                <div class="card-footer">
                  <button type="submit" class="btn btn-primary pull-right">Submit</button>
                </div>
              </form>
              </div>
            </div>
          </div>
          
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
@endsection



