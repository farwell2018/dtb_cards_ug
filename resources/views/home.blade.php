@extends('layouts.admin')

@section('header')

<div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            @if(Auth::user()->role_id === 'admin' )
            <h1 class="m-0 text-dark">Dashboard</h1>
            @endif
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item active"><a href="{{route('home')}}">Home</a></li>
              <!-- <li class="breadcrumb-item active">Starter Page</li> -->


            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->


@endsection

@section('content')

@if(Auth::user()->role_id === 'admin')

@include('includes.dashboard.admin')
@else
@include('includes.dashboard.associate')
@endif



@endsection