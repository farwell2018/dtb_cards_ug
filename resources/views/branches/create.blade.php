@extends('layouts.admin')

@section('header')
<div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Create Branch</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{route('home')}}">Home</a></li>
              <li class="breadcrumb-item active"><a href="{{route('branches.index')}}">Branches </a></li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->

@endsection

@section('content')
     <div class="container-fluid">
        <div class="row">
         <div class="col-12">
            <div class="card">
              <!-- /.card-header -->
              <div class="card-body">
               
               <form role="form" method="post" action="{{route('branches.store')}}">
                {{ csrf_field() }}
                <div class="card-body">
                  <div class="form-group">
                    <label for="exampleInputEmail1">Name</label>
                    <input type="text"  class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name')  }}" required>

                    @if ($errors->has('name'))
                    <span class="invalid-feedback" role="alert">
                      <strong>{{ $errors->first('name') }}</strong>
                    </span>
                    @endif
                  </div>
                      <div class="form-group ">
                        <label for="exampleInputEmail1">Status</label>
                            <select id="role" name="status" class="form-control{{ $errors->has('status') ? ' is-invalid' : '' }}" required>
                            <option disabled selected>Select status</option>
                            @foreach($statuses as $status)
                           
                             <option value="{{$status['id']}}">{{$status['name']}}</option>
                              
                            @endforeach
                            </select>
      
                                @if ($errors->has('status'))
                                <span class="invalid-feedback" role="alert">
                                  <strong>{{ $errors->first('status') }}</strong>
                                </span>
                                @endif
      
                          </div>
                </div>

               
                <!-- /.card-body -->
                <div class="card-footer">
                  <button type="submit" class="btn btn-primary pull-right">Submit</button>
                </div>
              </form>
              </div>
            </div>
          </div>
          
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
@endsection



