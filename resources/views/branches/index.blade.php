@extends('layouts.admin')

@section('header')
<div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Branches</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{route('home')}}">Home</a></li>
              <li class="breadcrumb-item active">Branches </li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
        <div class="row mb-2">
          <div class="col-sm-11">
          </div><!-- /.col -->
          
          @if(Auth::user()->role->name ==='Administator')
          <div class="col-sm-1">
            <a href="{{route('branches.create')}}" class="btn btn-block btn-primary"> Add Branch</a>
          </div><!-- /.col -->
          @endif
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->

@endsection

@section('content')
     <div class="container-fluid">
        <div class="row">
         <div class="col-12">
            <div class="card">
              <!-- /.card-header -->
              <div class="card-body">
               <table class="table table-bordered table-striped table-hover dt-responsive" id="branches" width="100%">
                <thead>
                  <tr>
                    <th>#</th>
                    <th>Name</th>
                    <th>Status</th>
                    <th>Tool</th>
                  </tr>
                </thead>
              </table>
              </div>
            </div>
          </div>

        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
@endsection

@section('footer-js')
<script>
$(document).ready(function(){
  $('#branches').DataTable({
    responsive: true,
    "deferRender": true,
    "processing": true,
    "serverSide": true,
    "ordering": true, //disable column ordering
    "lengthMenu": [
      [5, 10, 15, 20, 25, -1],
      [5, 10, 15, 20, 25, "All"] // change per page values here
    ],
    "pageLength": 100,
    "ajax": {
      url: '{!! route('branches.json') !!}',
      method: 'GET'
    },
    // dom: '<"html5buttons"B>lTfgitp',
    "dom": "<'row' <'col-md-12'>><'row'<'col-md-8 col-sm-12'lB><'col-md-4 col-sm-12'f>r><'table-scrollable't><'row'<'col-md-5 col-sm-12'i><'col-md-7 col-sm-12'p>>", // horizobtal scrollable datatable
    buttons: [
      { extend: 'copy',exportOptions: {columns: [0, 1, 2, 3,4,5,6]}},
      {extend: 'csv',  title: '{{ config('app.name', 'DTB') }} - List of Bank Branches',exportOptions: {columns: [0, 1, 2]}},
      {extend: 'excel', title: '{{ config('app.name', 'DTB') }} - List of Bank Branches',exportOptions: {columns: [0, 1, 2]}},
      {extend: 'pdf', title: '{{ config('app.name', 'DTB') }} - List of Bank Branches',exportOptions: {columns: [0, 1, 2]}},
      {extend: 'print',
      customize: function (win){
        $(win.document.body).addClass('white-bg');
        $(win.document.body).css('font-size', '10px');
        $(win.document.body).find('table')
        .addClass('compact')
        .css('font-size', 'inherit');
      }
    }
  ],
  columns: [
    {data: 'id',render: function (data, type, row, meta) {
        return meta.row + meta.settings._iDisplayStart + 1;
      }},
    {data: 'name', name: 'name', orderable: true, searchable: true},
    {data: 'status', name: 'status', orderable: true, searchable: true},
    {data: 'tools', name: 'tools', orderable: false, searchable: false,"defaultContent": "<i>Not set</i>"},
  ],
});
});

</script>

@endsection