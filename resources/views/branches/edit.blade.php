@extends('layouts.admin')

@section('header')
<div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Edit: {{ $branch->first_name  }}</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{route('home')}}">Home</a></li>
              <li class="breadcrumb-item active"><a href="{{route('branches.index')}}">Users </a></li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->

@endsection

@section('content')
     <div class="container-fluid">
        <div class="row">
         <div class="col-12">
            <div class="card">
              <!-- /.card-header -->
              <div class="card-body">

               <form role="form" method="post" action="{{ route('branches.update',$branch->id) }}">
                 @method('PUT')
                 @csrf
                <div class="card-body">
                  <div class="form-group">
                    <label for="exampleInputEmail1">Name</label>
                    <input type="text"  class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ $branch->name  }}" required>

                    @if ($errors->has('name'))
                    <span class="invalid-feedback" role="alert">
                      <strong>{{ $errors->first('name') }}</strong>
                    </span>
                    @endif
                  </div>
              
                          <div class="form-group ">
                            <label for="exampleInputEmail1">Status</label>
                                <select id="status" name="status" class="form-control{{ $errors->has('status') ? ' is-invalid' : '' }}" required>
                                <option disabled >Select Status</option>
                                @foreach($statuses as $status)
                                @if($status['id'] === $branch->status)
                                  <option value="{{$status['id']}}" selected="selected">{{$status['name']}}</option>
                                  @else
                                 <option value="{{$status['id']}}">{{$status['name']}}</option>
                                  @endif
                                @endforeach
                                </select>
    
                                    @if ($errors->has('status'))
                                    <span class="invalid-feedback" role="alert">
                                      <strong>{{ $errors->first('status') }}</strong>
                                    </span>
                                    @endif
    
                              </div>
                          
                </div>
                <!-- /.card-body -->
                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Submit</button>
                </div>
              </form>
              </div>
            </div>
          </div>

        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
@endsection
