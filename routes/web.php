<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'WelcomeController@index');

Route::get('/cities/{id}', 'WelcomeController@getCities')->name('cities');

Route::post('/card', 'WelcomeController@cardInformation')->name('card.information');

Route::get('refresh-captcha', 'WelcomeController@refreshCaptcha')->name('refreshCaptcha');
Route::get('/auth-permissions', 'WelcomeController@authPremissions')->name('auth-premissions');

Route::middleware(['checkIp'])->group(function () {
    Route::get('/login', 'Auth\LoginController@showLoginForm')->name('login');
    Route::post('/login', 'Auth\LoginController@login');
    Route::post('/logout', 'Auth\LoginController@logout')->name('logout');


    Route::get('/site/account_verify/{id}', 'Auth\VerificationController@accountVerify')->name('site-account_verify');
    Route::any('/site/account_verify', 'Auth\VerificationController@accountVerification')->name('site-account_verification');


    Route::get('/home', 'HomeController@index')->name('home');

    Route::get('/no-permissions', 'HomeController@noPremissions')->name('no-premissions');
});

Route::middleware(['auth','admin','checkIp'])->prefix('admin')->group(function () {
    Route::get('users/json', 'UserController@json')->name('users.json');
    Route::resource('users', 'UserController');
    Route::get('branches/json', 'BranchController@json')->name('branches.json');
    Route::resource('branches', 'BranchController');
    Route::get('logs', 'HomeController@logsIndex')->name('logs.index');
});

Route::middleware(['auth','associate','checkIp'])->prefix('staff')->group(function () {
    Route::get('information/json', 'InfoController@json')->name('information.json');
    Route::get('information', 'InfoController@index')->name('information.index');


    Route::post('logs', 'InfoController@storeLogs')->name('logs.store');
});
