<?php

namespace App\Imports;

use App\Models\Region;
use App\Models\Tier;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;

class RegionsImport implements ToCollection
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function collection(Collection $collection)
    {
        return $collection->map(function($region){

            $tier = Tier::where('name', $region[3])->first();

            $region = Region::firstOrCreate([
                'region' => $region[1]
            ]);

            if($region->save())
            {
                $region->update([
                    'tier' => $tier->id
                ]);
            }
        });
    }
}
