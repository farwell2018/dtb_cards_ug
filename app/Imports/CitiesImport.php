<?php

namespace App\Imports;

use App\Models\City;
use App\Models\Region;
use App\Models\Tier;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;

class CitiesImport implements ToCollection
{
    public function collection(Collection $collection)
    {
        return $collection->map(function ($city) {
            $region = Region::where('region', $city[1])->first();

            $tier = Tier::where('name', $city[3])->first();

            $cities = City::firstOrCreate([
                'region_id' => $region->id,
                'city_name' => $city[0]
            ]);

            if($cities->save()){
                $cities->update([
                    'tier' => $tier->id
                ]);
            }
        });
    }
}
