<?php

namespace App\Imports;

use App\Models\Tier;
use Maatwebsite\Excel\Concerns\ToModel;

class TiersImport implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        // dd($row);
        return Tier::firstOrCreate([
            'name' => $row[3]
        ]);
    }
}
