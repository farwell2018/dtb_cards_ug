<?php

namespace App\Imports;

use App\Models\Branch;
use Maatwebsite\Excel\Concerns\ToModel;

class BranchesImport implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return Branch::firstOrCreate([
            'name' => $row[1],
            'branch_code' => $row[0],
            'status' => 1
        ]);
    }
}
