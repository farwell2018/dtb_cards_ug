<?php

namespace App\Http\Middleware;

use Closure;

class CheckIpMiddleware
{
    public $whiteIps = ['102.140.218.111', '102.140.219.194', '102.222.74.174', '102.222.74.74', '102.222.75.175', '102.222.75.75'];
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!in_array($request->ip(), $this->whiteIps)) {

            /*
                 You can redirect to any error page.
            */

            return redirect('/auth-permissions');
        }

        return $next($request);
    }
}
