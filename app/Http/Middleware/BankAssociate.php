<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class BankAssociate
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::check()) {

            $user = Auth::user();
            if($user->role_id === 2 || $user->role_id === 1 ){
                return $next($request);
            }
        }
        return redirect('/no-permissions');
    }
}
