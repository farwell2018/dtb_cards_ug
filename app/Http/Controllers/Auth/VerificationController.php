<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\UserVerify;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;


class VerificationController extends Controller
{
    //

    

    public function accountVerify(Request $request, $key){
        $user = User::where('auth_key', $key)->first();

        $userVerify = UserVerify::where('user_id', $user->id)->where('email',$user->email)->first();

        if(empty($user) || empty($userVerify)){
         Auth::logout();
          return redirect()->route("login");

        }else{

        $id = $user->id;
        return view("auth.account_verify",compact('id'));
        }
     }
 
 
     public function accountVerification(Request $request){

       $verify = UserVerify::where('user_id', $request->input('user_id'))->where('token',$request->input('code'))->first();

       $value = $request->cookie('referrer');
 
       if(empty($verify)){
         
        Auth::logout();
        return redirect()->route("login");

       }else{
        $user = User::where('id', $request->input('user_id'))->first();

        $credentials = ['email' => $user->email, 'password' => $value];

        if (Auth::attempt($credentials)) {
        
          $user->last_login_date = Carbon::now();
          $user->save();


          $verify->delete();
          return redirect()->route("home");

        }else{

          return redirect()->route("login")->with('error', 'The username or password you entered did not match our records. Please confirm you have the right credentials ');
        }
             
       }
     }
}
