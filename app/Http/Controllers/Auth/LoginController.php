<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Mail;
use App\Models\UserVerify;
use App\Models\User;
use Cookie;
use Hash;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }




    public function login(Request $request)
    {
      $data = $request->all();
  
      $validator = Validator::make($data, [
      'email'    => 'required',
      'password' => 'required|string|min:8',
      ]);
      
    $credentials = ['email' => $request->input('email'), 'password' => $request->input('password')];
    $user = User::where('email', $request->input('email'))->first();

     // validate Credentials
      if ($validator->fails()) {
        $errors = $validator->errors();
  
        return redirect()->back()->with('error', $errors);
      }
  
       // Attempt Login
       if (Auth::attempt($credentials)) {
   
       if (empty($user->email_verified_at)) {

       Auth::logout();
       return Redirect::back()->with('error', 'Kindly verify your email first before proceeding');

       }elseif($user->status === 2){
          Auth::logout();
          return redirect()->back()->with('error', 'Your account is not active');

      }else{

        return redirect()->route("home");

      }
   
      }else{
        Auth::logout();
     return redirect()->back()->with('error', 'The email or password you entered did not match our records. Please confirm you have the right credentials ');
      }

    //   $user = User::where('email', $request->input('email'))->first();

    // if(!Hash::check($request->input('password'), $user->password)){
    //   Auth::logout();
    //   return redirect()->back()->with('error', 'The email or password you entered did not match our records. Please confirm you have the right credentials ');
    
    // }elseif (empty($user->email_verified_at)) {
    //   Auth::logout();
    //   return redirect()->back()->with('error', 'Kindly verify your email first before proceeding');

    //   }elseif($user->status === 2){
    //   Auth::logout();
    //   return redirect()->back()->with('error', 'Your account is not active');
    //   }else{
       
    //     $verify = UserVerify::where('user_id',$user->id)->where('email', $user->email)->first();
        
    //     if($verify){
    //       $verify->delete();
    //     }
        
    //     $userverify = new UserVerify();
    //     $userverify->user_id = $user->id;
    //     $userverify->email = $user->email;
    //     $userverify->token = self::generateRandomString(16);
    //     $userverify->save();


    //    Mail::send('emails.account_verify',['name' => $user->first_name.' '.$user->last_name, 'code' => $userverify->token, 'url' => route('site-account_verify', $user->auth_key)],
    //     function ($message) use ($user) {
    //       $message->from('support@farwell-consultants.com', 'The Card Delivery Account Verification');
    //       $message->to($user->email, $user->first_name);
    //       $message->subject('The Card Delivery Account Verification');
    //     });

    //       return redirect()->route("site-account_verify",[$user->auth_key])->withCookie(cookie('referrer', $request->input('password'), 10));
        
    //   }
     
    }

    public function logout(Request $request)
    {
      
        $this->guard()->logout();
        return redirect()->route('login');
      }

    protected function generateRandomString($length = 10)
        {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[ rand(0, $charactersLength - 1) ];
        }

        return $randomString;
        }
}
