<?php

namespace App\Http\Controllers;

use App\Imports\BranchesImport;
use App\Imports\CitiesImport;
use App\Imports\RegionsImport;
use Illuminate\Http\Request;
use App\Models\Branch;
use App\Models\CardInformation;
use App\Models\BranchAccount;
use App\Models\City;
use App\Models\InformationBranch;
use App\Models\Region;
use Validator;
use Session;
use Redirect;
use Captcha;
use Maatwebsite\Excel\Facades\Excel;

class WelcomeController extends Controller
{
    //

    public function index(){

        // $excl = Excel::import(new RegionsImport,'data.xlsx');

        $branches = Branch::where('status','=', 1)->orderBy('name','Asc')->get();

        $regions = Region::orderBy('region','Asc')->get();

        return view('welcome', compact('branches', 'regions'));

    }


    public function cardInformation(Request $request){


        // dd($data);
        $country_code = '+256';

        $mobile = $country_code.''.$request->input('mobile_number');
        $request->merge([
            'phone_number' => $mobile,
        ]);

        //$data = $request->all();

        $validator = Validator::make($request->all(), [
            'first_name' => ['required', 'string', 'max:255'],
            'last_name' => ['required', 'string', 'max:255'],
            'email' => ['required','email', 'unique:users'],
            'phone_number'=> ['required','min:9','max:14'],
            'delivery_method' =>['required'],
            'captcha' => ['required','captcha'],
        ],
        [
            'required'  => 'The :attribute field is required.',
            'unique'    => 'A submission with this :attribute has already been received.',
            'captcha' =>  'The :attribute entered is wrong.'
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            return Redirect::back()->withInput()->withErrors($errors);
          }


        $card = new CardInformation();
        $card->first_name = $request->input('first_name');
        $card->last_name = $request->input('last_name');
        $card->email = $request->input('email');
        $card->phone_number =$request->input('phone_number');
        $card->delivery_method = $request->input('delivery_method');
        $card->branch = $request->input('branch');
        $card->home_address = $request->input('home');
        $card->road_near = $request->input('road_near');
        $card->landmark_near = $request->input('landmark_near');
        $card->city_town = $request->input('city_town');
        $card->region_id = $request->input('region');
        $card->city_id = $request->input('city');
        $card->status = 1;

        $card->save();


        $requirement= $_POST['Question'];

        foreach ($requirement as $req){
            if(!empty($req['Question'])){
            $quest = new InformationBranch();
            $quest->information_id = $card->id;
            $quest->branch = $req['Question'];
            $quest->save();
            }
        $tog = $req['Answers'];
      foreach ($tog as $key=>$value)
            {
            if(!empty($value)){
              $answers = new BranchAccount();
              $answers->account_number = $value;
              $answers->branch_id = $quest->id;
              $answers->information_id = $card->id;
              $answers->save();
            }
             }

        }

        $submission_success = [
            'content' => 'Thank you for your response. This information will help us deliver your new DTB Card.',
        ];

        Session::flash('submission_success', $submission_success);

        return Redirect::back();

    }


    public function refreshCaptcha()
    {
        return Captcha::img();
    }

    public function authPremissions(){

        return view('auth.no-permissions');
    }

    public function getCities($id)
    {
        $cities = City::where('region_id', $id)->get();

        return $cities;
    }
}
