<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\CardInformation;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Collection;
use Carbon\Carbon;
use App\Models\Log;
use App\Models\Branch;
use App\Models\City;
use App\Models\Region;

class InfoController extends Controller
{
    //

    public function index()
    {
        $branches = Branch::where('status', '=', 1)->orderBy('name', 'Asc')->get();
        return view('information.index', compact('branches'));
    }


    public function json(Request $request)
    {
        $cards = CardInformation::select("*")->orderBy('created_at', 'desc')->get();

        // dd($cards);

        foreach ($cards as $key => $card) {


            //By Status
            $delivery = $request->input('delivery');
            if ($delivery) {
                if ($card->delivery_method != $delivery) {
                    unset($cards[$key]);
                }
            }

            $branch = $request->input('branch');
            if ($branch) {
                if ($card->branch != $branch) {
                    unset($cards[$key]);
                }
            }
            //By range
            $range = $request->input('range');

            if ($range) {
                if (empty($card->created_at)) {
                    unset($cards[$key]);
                } else {
                    $range = explode(' - ', $range);
                    $isBetween = $card->created_at->isBetween(
                        Carbon::parse($range[0]),
                        Carbon::parse($range[1])->addHours(23)->format('Y-m-d H:i:s')
                    );
                    if ($isBetween == false) {
                        unset($cards[$key]);
                    }
                }
            }
        }

        $query = new Collection($cards);

        return Datatables::of($query)
       ->addIndexColumn()
       ->addColumn('first_name', function ($list) {
           return $list->first_name;
       })->addColumn('last_name', function ($list) {
           return $list->last_name;
       })->addColumn('region', function ($region) {
           return Region::select('region')->where('id', $region->region_id)->pluck('region')->toArray();
       })->addColumn('city', function ($city) {
           return City::select('city_name')->where('id', $city->city_id)->pluck('city_name')->toArray();
       })->addColumn('branch', function ($branch) {
           return Branch::select('name')->where('id', $branch->branch)->pluck('name')->toArray();
       })->addColumn('account_number', function ($list) {
           $acc = array();
           foreach ($list->Accounts as $info) {
               $acc[] =  $info->account_number ;
           }
           return  implode(",\n", $acc);
       })->addColumn('delivery_method', function ($list) {
           if ($list->delivery_method == 1) {
               return 'Collect from branch';
           } else {
               return 'Door Deliver';
           }
       })->addColumn('location', function ($list) {
           if ($list->delivery_method == 1) {
               if ($list->branches) {
                   return $list->branches->name;
               } else {
                   return ;
               }
           } else {
               return $list->home_address;
           }
       })
         ->make();
    }

    public function storeLogs(Request $request)
    {
        $log = new Log();
        $log->download_by = $request->input('download_by');
        $log->save();


        return response()->json(['success' => 'Log saved'], 200);
    }
}
