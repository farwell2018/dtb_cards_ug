<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Branch;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Str;
use Validator;
use Session;
use Redirect;
use Hash;
use Mail;
use Auth;
use DB;
use Carbon\Carbon;

class BranchController extends Controller
{
    //
    public function index(){
        return view('branches.index');
  
      }
      public function json(){
  
        $query = Branch::all();
  
       return Datatables::of($query)
       
       ->addColumn('status',function($list){
         if($list->status == 1){
         
           return 'Active';
         }else{
           return 'Inactive';
         }
  
       })
    
         ->addColumn('tools', function ($list) {
  
              return '
         <span style="overflow: visible; position: relative; width: 110px;">
  
           <a title="Edit details" class="btn btn-sm btn-clean btn-icon btn-icon-sm" href="' . route('branches.edit', $list->id) . '">
             <i class="fas fa-edit"></i>
           </a>
           <a
             title="Delete"
             class="btn btn-sm btn-clean btn-icon btn-icon-sm  delete-action"
             data-href="'.route('branches.destroy', $list->id).'"
             data-toggle="modal"
             data-target="#deleteConfirmModal"
             >
             <i class="far fa-trash-alt"></i>
           </a>
  
       </span>';
  
         })
         ->rawColumns(['tools'])
         ->make();
  
      }
  
  
    public function create(){
     
      $statuses = Branch::getStatus();
      return view('branches.create', compact('statuses'));
  
    }
  
  
    public function store(Request $request){
      $data = $request->all();
      $validator = Validator::make($data, [
          'name' => ['required', 'string', 'max:255'],
          'status' =>['required']
      ]);
   
      if ($validator->fails()) {
        $errors = $validator->errors();
        return Redirect::back()->withErrors($errors);
      }
  
  
        $branch = new Branch();
        $branch->name = $data['name'];
        $branch->status = $data['status'];
        $branch->save();
  
  
        return redirect()->route('branches.index');
  
    }
  
  
      public function edit($id){
         $branch = Branch::where('id',$id)->first();
         $statuses = Branch::getStatus();
        return view('branches.edit', compact('branch','statuses'));
   }
  
   public function update(Request $request, $id){
     $data = $request->all();
     $validator = Validator::make($data, [
      'name' => ['required', 'string', 'max:255'],
      'status' =>['required']
  ]);
  
     if ($validator->fails()) {
       $errors = $validator->errors();
       return Redirect::back()->withErrors($errors);
     }
  
     $branch = Branch::where('id',$id)->first();
     $branch->name = $data['name'];
     $branch->status = $data['status'];
  
     $branch->save();
  
      return redirect()->route('branches.index');
  
   }
  
   public function destroy(Request $request, $id){
     Branch::where('id',$id)->delete();
  
     return redirect()->route('branches.index');
  
   }

    
}
