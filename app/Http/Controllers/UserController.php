<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Role;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Str;
use Validator;
use Session;
use Redirect;
use Hash;
use Mail;
use Auth;
use DB;
use Carbon\Carbon;

class UserController extends Controller
{
   

    public function index(){
      return view('users.index');

    }
    public function json(){

      $query = User::where('role_id','!=',1)->get();

     return Datatables::of($query)
     ->addColumn('name',function($list){
       
       return $list->first_name.' '.$list->last_name;
       
     })
     ->addColumn('status',function($list){
       if(!empty($list->status === 1)){
       
         return 'Active';
       }else{
         return 'Inactive';
       }

     })
  
       ->addColumn('tools', function ($list) {

            return '
       <span style="overflow: visible; position: relative; width: 110px;">

         <a title="Edit details" class="btn btn-sm btn-clean btn-icon btn-icon-sm" href="' . route('users.edit', $list->id) . '">
           <i class="fas fa-edit"></i>
         </a>
         <a
           title="Delete"
           class="btn btn-sm btn-clean btn-icon btn-icon-sm  delete-action"
           data-href="'.route('users.destroy', $list->id).'"
           data-toggle="modal"
           data-target="#deleteConfirmModal"
           >
           <i class="far fa-trash-alt"></i>
         </a>

     </span>';

       })
       ->rawColumns(['tools'])
       ->make();

    }


  public function create(){
    $roles = Role::all();
    $statuses = User::getStatus();
    return view('users.create', compact('roles','statuses'));

  }


  public function store(Request $request){
    $data = $request->all();
    $validator = Validator::make($data, [
        'first_name' => ['required', 'string', 'max:255'],
        'last_name' => ['required', 'string', 'max:255'],
        'email' => ['required','email'],
        'password' =>['required','min:8'],
        'role' =>['required'],
        'status' =>['required']
    ]);
 
    if ($validator->fails()) {
      $errors = $validator->errors();
      return Redirect::back()->withErrors($errors);
    }


      $user = new User();
      $user->first_name = $data['first_name'];
      $user->last_name = $data['last_name'];
      $user->email = $data['email'];
      $user->password =  Hash::make($data['password']);
      $user->auth_key = self::generateRandomString(32);
      $user->role_id = $data['role'];
      $user->status = $data['status'];
      $user->email_verified_at = Carbon::now();

      $user->save();


      return redirect()->route('users.index');

  }


    public function edit($id){
       $user = User::where('id',$id)->first();
       $roles = Role::all();
       $statuses = User::getStatus();
      return view('users.edit', compact('user','roles','statuses'));
 }

 public function update(Request $request, $id){
   $data = $request->all();
   $validator = Validator::make($data, [
    'first_name' => ['required', 'string', 'max:255'],
    'last_name' => ['required', 'string', 'max:255'],
    'email' => ['required','email'],
    'role' =>['required'],
    'status' =>['required']
]);

   if ($validator->fails()) {
     $errors = $validator->errors();
     return Redirect::back()->withErrors($errors);
   }

   $user = User::where('id',$id)->first();
   $user->first_name = $data['first_name'];
   $user->last_name = $data['last_name'];
   $user->email = $data['email'];
   $user->password = $request->has('password') ? Hash::make($data['password']) : $user->password;
   $user->role_id = $data['role'];
   $user->status = $data['status'];

   $user->save();

    return redirect()->route('users.index');

 }

 public function destroy(Request $request, $id){

   VerifyUser::where('user_id',$id)->delete();
   User::where('id',$id)->delete();

   return redirect()->route('users.index');

 }

 protected function generateRandomString($length = 10)
 {
 $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
 $charactersLength = strlen($characters);
 $randomString = '';
 for ($i = 0; $i < $length; $i++) {
     $randomString .= $characters[ rand(0, $charactersLength - 1) ];
 }

 return $randomString;
 }

}
