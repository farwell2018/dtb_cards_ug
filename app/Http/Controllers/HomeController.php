<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\UserVerify;
use App\Models\CardInformation;
use App\Models\User;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cards = CardInformation::all();
        $BDelivery = CardInformation::where('delivery_method',1)->get();
        $HDelivery = CardInformation::where('delivery_method',2)->get();
        $users = User::all();

        return view('home', compact('cards','users','BDelivery','HDelivery'));
    }


    public function noPremissions(){

        return view('users.no_permissions');
    }
}
