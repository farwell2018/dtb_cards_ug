<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserVerify extends Model
{
    //
    protected $table = 'user_verification';

    protected $fillable = ['token','user_id','email'];
}
