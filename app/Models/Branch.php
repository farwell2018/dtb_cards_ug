<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Branch extends Model
{
    //

    const ACTIVE_USER = 1;
    const INACTIVE_USER = 2;


    protected $fillable = [
        'name',  'status', 'branch_code'
    ];



    public static function getStatus()
    {
        $statuses = array(
         array('id' => self::ACTIVE_USER, 'name'=>'Active'),
         array('id' => self::INACTIVE_USER, 'name'=>'Inactive'),
          );
        return $statuses;
    }


    /**
     * Set Branch Name Attribute
     *
     * @return string
     */
    public function setNameAttribute($name)
    {
        return $this->attributes['name'] = $name;
    }

    /**
     * Get name Attribute
     */
    public function getNameAttribute($name)
    {
        return ucwords(strtolower($name));
    }

    public function Delivery()
    {
        return $this->hasMany(Branch::class, 'id', 'branch');
    }

    public function InformationBranch()
    {
        return $this->hasMany(InformationBranch::class, 'id', 'information_id');
    }
}
