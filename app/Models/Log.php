<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Log extends Model
{
    //
    protected $table = "logs";

    public function users()
    {
        return $this->belongsTo(User::class,'download_by','id');
    }
}
