<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CardInformation extends Model
{
    //

    protected $table = 'card_information';
 
    protected $fillable = [
        'first_name', 'last_name',  'email','phone_number', 'delivery_method', 'branch', 'home_address', 'status'
      ];

    public function Branches()
    {
        return $this->belongsTo(Branch::class,'branch','id');
    }

    public function Infos()
    {
        return $this->hasMany(InformationBranch::class,'information_id','id');
    }


    public function Accounts()
    {
        return $this->hasMany(BranchAccount::class,'information_id','id');
    }
}
