<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;


    /**
     * Set default user status
     */
    const ACTIVE_USER = 1;
    const INACTIVE_USER = 2;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name', 'last_name','email', 'password', 'role_id', 'status', 'isban', 'last_login_date','auth_key'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];


     /**
     * A user has a single role
     */
    public function role()
    {
        return $this->hasOne(Role::class,'id','role_id');
    }
    
    public function logs()
    {
        return $this->hasMany(Log::class,'id','download_by');
    }

    public static function getStatus()
    {
        $statuses = array(
         array('id' => self::ACTIVE_USER, 'name'=>'Active'),
         array('id' => self::INACTIVE_USER, 'name'=>'Inactive'),
          );
        return $statuses;
    }
  
}
