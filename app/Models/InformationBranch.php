<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class InformationBranch extends Model
{
    //
    protected $table = 'information_branches';

    public function Informations()
    {
        return $this->belongsTo(CardInformation::class,'information_id','id');
    }


    public function Branches()
    {
        return $this->belongsTo(Branch::class,'branch','id');
    }
}
