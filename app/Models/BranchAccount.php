<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BranchAccount extends Model
{
    //

    protected $table = 'branch_accounts';


    public function Informations()
    {
        return $this->belongsTo(CardInformation::class,'information_id','id');
    }
}
