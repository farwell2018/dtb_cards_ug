<?php

return [
  /*
    Let's create the nav structure of the admin section.
  */
  'admin_nav' => [
    'Dashboard'=>[
      // 
      ['title'=>'Home','icon'=>'<i class="fas fa-home"></i>','route'=>'home'],
      ['title'=>'Card Information','icon'=>'<i class="fa fa-address-card"></i>','route'=>'information.index'],
      ['title'=>'Branches','icon'=>'<i class="fa fa-building"></i>','route'=>'branches.index'],
      ['title'=>'Users','icon'=>'<i class="fa fa-users"></i>','route'=>'users.index'],
      
      
    ],
    

  ],
  'BankAssociate_nav' => [
    'Dashboard'=>[
       ['title'=>'Home','icon'=>'<i class="fas fa-home"></i>','route'=>'home'],
       ['title'=>'Card Information','icon'=>'<i class="fa fa-address-card"></i>','route'=>'information.index'],
    ],
  ],




  /*
    Theme specific styles
  */
  'styles'=>[

    "admin-assets/plugins/fontawesome-free/css/all.min.css",
    "admin-assets/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css",
    "admin-assets/plugins/icheck-bootstrap/icheck-bootstrap.min.css",
    "admin-assets/plugins/jqvmap/jqvmap.min.css",
    "admin-assets/dist/css/adminlte.min.css",
    "admin-assets/plugins/overlayScrollbars/css/OverlayScrollbars.min.css",
    "admin-assets/plugins/daterangepicker/daterangepicker.css",
    "admin-assets/plugins/summernote/summernote-bs4.css",
    "admin-assets/plugins/datatables/dataTables.bootstrap4.min.css",
    "css/main.css",

  	//Global Mandatory Vendors

  ],

  /*
    Theme specific js
  */
  'js'=>[

  "admin-assets/plugins/jquery/jquery.min.js",
  "admin-assets/plugins/bootstrap/js/bootstrap.bundle.min.js",
  "admin-assets/plugins/chart.js/Chart.min.js",
  "admin-assets/plugins/sparklines/sparkline.js",
  "admin-assets/plugins/moment/moment.min.js",
  "admin-assets/plugins/daterangepicker/daterangepicker.js",
  "admin-assets/plugins/summernote/summernote-bs4.min.js",
  "admin-assets/dist/js/adminlte.min.js",
  "admin-assets/plugins/datatables/jquery.dataTables.min.js",
  "admin-assets/plugins/datatables/dataTables.bootstrap4.min.js",
  "admin-assets/plugins/datatables-responsive/js/dataTables.responsive.min.js",
  "admin-assets/plugins/datatables-responsive/js/responsive.bootstrap4.min.js",
  "js/admin.js",

  ]

];
