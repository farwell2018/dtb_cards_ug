<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsToCardInformationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('card_information', function (Blueprint $table) {
            $table->integer('region_id')->after('status')->nullable();
            $table->integer('city_id')->after('region_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('card_information', function (Blueprint $table) {
            $table->dropColumn('region_id');
            $table->dropColumn('city_id');
        });
    }
}
