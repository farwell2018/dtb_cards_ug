<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //

        $users = [
            ['first_name' => 'Portal',
            'last_name' => 'Administrator',
            'email' => 'admin@farwell-consultants.com',
            'password' => bcrypt('gilberts'),
            'role_id' => 1,
            'status' => 1,
            'email_verified_at'=> Carbon::now(),
            'auth_key' => self::generateRandomString(32),
            
        ]];

        foreach ($users as $user) {
            \App\Models\User::Create($user);
        }
    }



    protected function generateRandomString($length = 10)
    {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[ rand(0, $charactersLength - 1) ];
    }

    return $randomString;
    }
}
