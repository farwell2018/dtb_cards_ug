<?php

use Illuminate\Database\Seeder;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $roles = [
            ['name' => 'Administator'],
            ['name' => 'Staff'],
            

        ];
        foreach ($roles as $role) {
            \App\Models\Role::Create($role);
        }
    }
}
